const fs = require('fs')
const htmlParser = require('node-html-parser')
const FILENAME = './pdfConvertedToHtml.html'

const fileContent = fs.readFileSync(FILENAME, 'utf8')

const fileParsed = htmlParser.parse(fileContent)
const allDivs = fileParsed.querySelectorAll('div')

const parseDiv = (divText) => {
  if (startsWithLetterAndParenthesis(divText)) {
    return 'ANSWER'
  } else if (startsWith4NumbersAndADot(divText)) {
    return 'QUESTION'
  } else {
    return 'TEXT'
  }
}

const startsWithLetterAndParenthesis = (text) => {
  return text[1] === ')' && ['A', 'B', 'C', 'D'].includes(text[0])
}

const startsWith4NumbersAndADot = (text) => {
  return text[4] === '.' && text.match(/^\d/)
}

const questions = []
let question = {}
let previousType = ''
let previousAnswerIndex = 0

const appendQuestion = () => {
  if (question.text && question.text !== '') {
    questions.push(question)
  }
}

allDivs.forEach((div) => {
  const divText = div.rawText
  const textType = parseDiv(divText)

  if (textType === 'TEXT' && previousType === 'QUESTION') {
    question.text = question.text + divText
  } else if (textType === 'QUESTION' && (previousType === 'TEXT' || previousType === 'ANSWER')) {
    appendQuestion()
    previousAnswerIndex = 0
    question = {
      number: Number(divText.slice(0, 4)),
      text: divText,
      answers: []
    }
  } else if (textType === 'TEXT' && previousType === 'ANSWER') {
    if (!divText.startsWith('Pagina')) {
      question.answers[previousAnswerIndex] = question.answers[previousAnswerIndex] + divText
    }
  } else if (textType === 'ANSWER') {
    question.answers[previousAnswerIndex++] = divText.slice(3)
  }

  previousType = textType
})

fs.writeFile('questions.js', `module.exports = ${JSON.stringify(questions)}`, (err) => {
  if (err) {
    throw err
  }
  console.log('JSON data is saved.')
})
