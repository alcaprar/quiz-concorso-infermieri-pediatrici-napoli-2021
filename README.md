# README

## Local development

Install packages:
```
yarn install
```

Start a simple http server:

```
cd src
python3 -m http.server
```

When you want to see your updated code, just refresh :).